package dg.blue.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class AuthController {

  private static final Logger log = LoggerFactory.getLogger(AuthController.class);

  @PostMapping("/auth")
  public AuthRequest auth(@RequestBody AuthRequest req) {
    // generate GUID
    String uuid = UUID.randomUUID().toString();
    req.setId(uuid);

    log.info("Incoming request: " + req);

    // todo validate

    // todo generate response

    return req;
  }
}
